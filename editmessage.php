<?php
session_start();
header('content-type:text/html;charset=utf-8');
if (!empty($_SESSION['username']) and !empty($_SESSION['userpassword'])) {
    $boardmeid = "";
    $boardmeid = $_GET['meid'];//取網址meid值
    $suc = "";//上傳檔案名稱預設空
    $okmessage = "";//成功修改預設空
    try {
        $pdo = new PDO("mysql:host=localhost;dbname=firstlab;", "root", "");
    } catch (PDOException $err) {
        die("資料庫無法連接");
    }
    //登入時間超過15分鐘,刪除session
    if (isset($_SESSION['start']) && (time() - $_SESSION['start'] > 900)) {
        unset($_SESSION['username']);
        unset($_SESSION['userpassword']);
        header("Location:index.php");
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //登出
        if (isset($_POST["out"])) {
            unset($_SESSION['username']);
            unset($_SESSION['userpassword']);
            header("Location:index.php");
        }
        //修改留言
        if (isset($_POST['remessage'])) {
            if (!empty($_POST['checkbox'])) {//如果有刪除檔案
                $arrlength = count($_POST['checkbox']);
                for ($x = 0; $x < $arrlength; $x++) {
                    $stmt = $pdo->prepare("select * from file where fiid=?");
                    $stmt->execute(array($_POST['checkbox'][$x]));
                    $rows = $stmt->fetchAll();
                    //刪除upload內指定資料
                    foreach ($rows as $r) {
                        $fileName = 'upload/' . $r['messageid'] . $r['filename'];
                        unlink($fileName);
                    }
                    //刪除資料庫留言板資料
                    $stmt = $pdo->prepare("delete from file where fiid=?");
                    $stmt->execute(array($_POST['checkbox'][$x]));
                }
            }
            //updata(留言)
            $stmt = $pdo->prepare("update message set content=? where mesid=?");
            $res = $stmt->execute(array($_POST["exampleFormControlTextarea2"], $boardmeid));
            # 取得上傳檔案數量
            $fileCount = count($_FILES['my_file']['name']);
            for ($i = 0; $i < $fileCount; $i++) {
                # 檢查檔案是否上傳成功
                if ($_FILES['my_file']['error'][$i] === UPLOAD_ERR_OK) {
                    $suc = $suc . $_FILES['my_file']['name'][$i] . "/";
                    $filename = $_FILES['my_file']['name'][$i];
                    $_FILES['my_file']['name'][$i] = $boardmeid . $_FILES['my_file']['name'][$i];
                    // echo '檔案名稱: ' . $_FILES['my_file']['name'][$i] . '<br/>';
                    // echo '檔案類型: ' . $_FILES['my_file']['type'][$i] . '<br/>';
                    // echo '檔案大小: ' . ($_FILES['my_file']['size'][$i] / 1024) . ' KB<br/>';
                    // echo '暫存名稱: ' . $_FILES['my_file']['tmp_name'][$i] . '<br/>';
                    # 檢查檔案是否已經存在
                    if (file_exists('upload/' . $_FILES['my_file']['name'][$i])) {
                        $message = $filename . "檔案已存在";
                        echo "<script type='text/javascript'>alert('$message');</script>";
                    } else {
                        $file = $_FILES['my_file']['tmp_name'][$i];
                        $dest = 'upload/' . $_FILES['my_file']['name'][$i];
                        //儲存檔名到資料庫
                        $stmt = $pdo->prepare("insert into file(messageid,filename) values(?,?)");
                        $stmt->execute(array($boardmeid, $filename));
                        # 將檔案移至指定位置
                        move_uploaded_file($file, $dest);
                    }
                } else {
                    // echo '錯誤代碼：' . $_FILES['my_file']['error'] . '<br/>';
                }
            }
            $okmessage = "修改完成，將回到留言板頁面";
        }
    }
    //顯示上傳檔案
    $stmt = $pdo->prepare("select * from file where messageid=?");
    $stmt->execute(array($boardmeid));
    $filedata = $stmt->fetchAll();
    //顯示留言
    $stmt = $pdo->prepare("select * from message where mesid=?");
    $stmt->execute(array($boardmeid));
    $mecontent = $stmt->fetchAll();
    //修改完成回留言板
    if ($okmessage == "修改完成，將回到留言板頁面") {
        echo "<script> alert('$okmessage'); </script>";
        echo "<meta http-equiv='Refresh' content='0;URL=http://localhost/board.php'>";
    }

?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="description" content="lab work">
        <meta name="author" content="Yi-Ling">
        <title>修改留言</title>
        <!--CDN via jsDelivr -->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
        <style>
            h3 {
                text-decoration-line: underline;
                text-decoration-style: double;
            }
        </style>
    </head>

    <body>
        <div class="container-md">
            <form method="post" action="">
                <div class="row" style="margin-top:20px;">
                    <h3 style="color: blue;text-align:center;">修改留言</h3>
                    <span style="text-align:right;"><button type="submit" class="btn btn-primary" id="out" name="out">登出</button></span>
                </div>
            </form>
            <div class="row" style="margin-top: 10px;margin-bottom:20px;">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <?php
                        //顯示留言
                        foreach ($mecontent as $q) {
                            echo '<textarea class="form-control" id="exampleFormControlTextarea2" name="exampleFormControlTextarea2" rows="1" placeholder="輸入留言內容">' . $q["content"] . '</textarea>';
                        }
                        ?>
                    </div>
                    
                    <div>已上傳檔案(勾選可刪除)</div>
                    <?php
                    foreach ($filedata as $e) {
                        echo '
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value=' . $e["fiid"] . ' id="checkbox[]" name="checkbox[]">
                            <label class="form-check-label" for="flexCheckDefault">
                            ' . $e["filename"] . '
                            </label>
                        </div>';
                        // echo '<option value=' . $e["fiid"] . '>' . $e["filename"] . '</option>';
                    }
                    ?>
            </div>
            <div class="mb-3">
                <input class="form-control form-control-sm" type="file" name="my_file[]" multiple>
            </div>
            <div>
                <button type="submit" class="btn btn-outline-primary btn-sm" id="remessage" name="remessage" style="float: right;">修改留言</button>
            </div>
            </form>
        </div>
        </div>
    </body>

    </html>
<?php
} else {
    header("Location:index.php");
}
?>