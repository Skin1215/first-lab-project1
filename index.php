<?php 
	session_start(); 
	try {
        $pdo=new PDO("mysql:host=localhost;dbname=firstlab;","root","");
    } catch (PDOException $err) { 
        die("資料庫無法連接"); 
    }
	$message ="";//空
	if ($_SERVER["REQUEST_METHOD"]=="POST") {
		//註冊
		if (isset($_POST["register"])) {
			$stmt=$pdo->prepare("insert into member(user,password) values(?,?)");
            $stmt->execute(array($_POST["username"],$_POST["password"]));
		}		
		if (isset($_POST["sendlogin"])) {
			$stmt=$pdo->prepare("select * from member");
   			$stmt->execute();
    		$rows=$stmt->fetchAll();
			foreach ($rows as $r) {
				if ($_POST["username"]==$r["user"]){//帳號正確(存在)
					$er=1;
					if ($_POST["password"]==$r["password"]){//密碼正確
						$_SESSION["username"]=$_POST["username"];//儲存帳號
						$_SESSION["userpassword"]=$_POST["password"];//儲存密碼
						$message = "登入成功";
						echo "<script type='text/javascript'>alert('$message');</script>";
						$_SESSION['start'] = time();//獲取登入時間
						break;  						
					}else{			
						$message = "密碼錯誤,登入失敗";
						echo "<script type='text/javascript'>alert('$message');</script>";
						break;
					}
				}else{
					$er=0;
					continue;
				}
			}
			if($er==0){
				$message = "帳號錯誤,登入失敗";
				echo "<script type='text/javascript'>alert('$message');</script>";
			}
		} 
	}

	// 登入成功跳頁
	if ($message == "登入成功") {
		header('Location:board.php');
	}

	
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="lab work">
    <meta name="author" content="Yi-Ling">
	<title>登入系統</title>
	<!--CDN via jsDelivr -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>	
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row justify-content-center align-items-center" style="height:100vh">
			<div class="col-4">
				<div class="card">
					<div class="card-body">
						<form action="index.php" method="post">
							<div class="form-group">
								<input type="text" class="form-control" id="username" name="username" placeholder="帳號">
							</div>
							<br>
							<div class="form-group">
								<input type="password" class="form-control" id="password" name="password" placeholder="密碼">
							</div>
							<br>
							 <div class="text-center">
							 	<button type="submit" id="sendlogin" name="sendlogin" class="btn btn-primary" style="width: 40%;">登入</button>
								<button type="submit" id="register" name="register" class="btn btn-primary" style="width: 40%;">註冊</button>
   							 </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>



