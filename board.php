<?php
session_start();
header('content-type:text/html;charset=utf-8');
if (!empty($_SESSION['username']) and !empty($_SESSION['userpassword'])) {;
    $suc = "";
    try {
        $pdo = new PDO("mysql:host=localhost;dbname=firstlab;", "root", "");
    } catch (PDOException $err) {
        die("資料庫無法連接");
    }
    //登入時間超過15分鐘,刪除session
    if (isset($_SESSION['start']) && (time() - $_SESSION['start'] > 900)) {
        unset($_SESSION['username']);
        unset($_SESSION['userpassword']);
        header("Location:index.php");
    }

    //獲取使用者編號
    $stmt = $pdo->prepare("select * from member where user=?");
    $stmt->execute(array($_SESSION['username']));
    $rows = $stmt->fetchAll();
    foreach ($rows as $r) {
        $userid = $r['memid'];
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //新增留言並上傳檔案
        if (isset($_POST["addmessage"])) {
            $stmt = $pdo->prepare("insert into message(memberid,content) values(?,?)");
            $stmt->execute(array($userid, $_POST["exampleFormControlTextarea1"]));
            //取message的id
            $stmt = $pdo->prepare("select * from message");
            $stmt->execute();
            $rows = $stmt->fetchAll(); //只有一筆則$rows=$stmt->fetch();
            foreach ($rows as $r) {
                $mesid = $r["mesid"];
            }
            # 取得上傳檔案數量
            $fileCount = count($_FILES['my_file']['name']);
            for ($i = 0; $i < $fileCount; $i++) {
                # 檢查檔案是否上傳成功
                if ($_FILES['my_file']['error'][$i] === UPLOAD_ERR_OK) {
                    $suc = $suc . $_FILES['my_file']['name'][$i] . "/";
                    $filename = $_FILES['my_file']['name'][$i];
                    $_FILES['my_file']['name'][$i] = $mesid . $_FILES['my_file']['name'][$i];
                    // echo '檔案名稱: ' . $_FILES['my_file']['name'][$i] . '<br/>';
                    // echo '檔案類型: ' . $_FILES['my_file']['type'][$i] . '<br/>';
                    // echo '檔案大小: ' . ($_FILES['my_file']['size'][$i] / 1024) . ' KB<br/>';
                    // echo '暫存名稱: ' . $_FILES['my_file']['tmp_name'][$i] . '<br/>';
                    # 檢查檔案是否已經存在
                    if (file_exists('upload/' . $_FILES['my_file']['name'][$i])) {
                        //echo "檔案已存在";
                    } else {
                        $file = $_FILES['my_file']['tmp_name'][$i];
                        $dest = 'upload/' . $_FILES['my_file']['name'][$i];
                        //儲存檔名到資料庫
                        $stmt = $pdo->prepare("insert into file(messageid,filename) values(?,?)");
                        $stmt->execute(array($mesid, $filename));
                        # 將檔案移至指定位置
                        move_uploaded_file($file, $dest);
                    }
                } else {
                    // echo '錯誤代碼：' . $_FILES['my_file']['error'] . '<br/>';
                }
            }
        }
        //登出
        if (isset($_POST["out"])) {
            unset($_SESSION['username']);
            unset($_SESSION['userpassword']);
            header("Location:index.php");
        }
        //修改留言
        if (isset($_POST['resend'])) {
            header("Location:editmessage.php?meid=" . $_POST["hidden"]);
        }

        //刪除留言和檔案
        if (isset($_POST['delete'])) {
            //取要刪除檔案
            $stmt = $pdo->prepare("select * from file where messageid=?");
            $stmt->execute(array($_POST['hidden']));
            $rows = $stmt->fetchAll();
            //刪除upload內指定資料
            foreach ($rows as $r) {
                $fileName = 'upload/' . $_POST['hidden'] . $r['filename'];
                unlink($fileName);
            }
            //刪除資料庫留言板資料
            $stmt = $pdo->prepare("delete from file where messageid=?");
            $stmt->execute(array($_POST['hidden']));
            $stmt = $pdo->prepare("delete from message where mesid=?");
            $stmt->execute(array($_POST['hidden']));
        }

        //下載檔案
        if (isset($_POST["download"])) {
            $zipName = 'document.zip';
            $zip = new \ZipArchive();
            if ($zip->open($zipName, \ZipArchive::CREATE) !== TRUE) {
                exit("cannot open <$zipName>\n");
            }
            $stmt = $pdo->prepare("select * from file where messageid=?");
            $stmt->execute(array($_POST["hidden"]));
            $rows = $stmt->fetchAll();
            foreach ($rows as $r) {
                $zip->addFile("upload/" . $_POST['hidden'] . $r['filename'] . "", $r['filename']);
            }
            $zip->close();
            // Download the created zip file
            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename = $zipName");
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile("$zipName");
            //delete this zip file after download
            unlink($zipName);
            exit;
        }
    }
    //顯示留言
    $stmt = $pdo->prepare("select * from member inner join message on member.memid=message.memberid ORDER BY mesid DESC");
    $stmt->execute();
    $message = $stmt->fetchAll();

?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="description" content="lab work">
        <meta name="author" content="Yi-Ling">
        <title>留言板</title>
        <!--CDN via jsDelivr -->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
        <script>
            updateList = function() {
                var input = document.getElementById('my_file[]');
                var output = document.getElementById('fileList');

                output.innerHTML = '<ul>';
                for (var i = 0; i < input.files.length; ++i) {
                    output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
                }
                output.innerHTML += '</ul>';
            }
        </script>
        <style>
            h3 {
                text-decoration-line: underline;
                text-decoration-style: double;
            }
        </style>
    </head>

    <body>
        <div class="container-md">
            <form method="post" action="board.php">
                <div class="row" style="margin-top:20px;">
                    <h3 style="color: blue;text-align:center;">留言板</h3>
                    <span style="text-align:right;"><button type="submit" class="btn btn-primary" id="out" name="out">登出</button></span>
                </div>
            </form>
            <div class="row" style="margin-top: 10px;margin-bottom:20px;">
                <form action="board.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="exampleFormControlTextarea1" rows="1" placeholder="輸入留言內容"></textarea>
                    </div>
                    <div class="mb-3">
                        <input class="form-control form-control-sm" type="file" name="my_file[]" id="my_file[]" multiple onchange="javascript:updateList()">
                    </div>
                    <br />已選擇檔案：
                    <div id="fileList"></div>

                    <div>
                        <button type="submit" class="btn btn-outline-primary btn-sm" id="addmessage" name="addmessage" style="float: right;">新增留言</button>
                    </div>
                </form>
            </div>

            <?php
            foreach ($message as $r) {
                //附加檔案一開始為空值
                $filename2 = "";
                $stmt = $pdo->prepare("select * from file where messageid=?");
                $stmt->execute(array($r['mesid']));
                $filedata = $stmt->fetchAll();
                foreach ($filedata as $e) {
                    $filename2 = $filename2 . $e["filename"] . "<br>";
                }
                if ($_SESSION['username'] == $r['user']) {
                    echo
                    '
                    <form method="post" enctype="multipart/form-data" action="board.php">
                        <input type="hidden" name="hidden" value=' . $r['mesid'] . '>
                        <div class="row">
                            <div class="col">
                                <div class="card" style="margin-bottom: 10px;">
                                    <h5 class="card-header">' . $r['user'] . '</h5>
                                    <div class="card-body">
                                        <p class="card-text" style="text-align:justify;">
                                        ' . $r['content'] . '
                                        </p>
                                        <button type="submit" class="btn btn-outline-secondary btn-sm" id="resend" name="resend">修改留言</button>
                                        <span><button type="submit" class="btn btn-outline-secondary btn-sm" id="delete" name="delete">刪除留言</button></span>
                                        <p style="margin-top: 20px;color:#0080FF;">附加檔案：</p>
                                        <p>' . $filename2 . '</p>
                                        <button type="submit" class="btn btn-outline-secondary btn-sm" id="download" name="download" onclick="upmeid(' . $r['mesid'] . ')">下載檔案</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>';
                } else {
                    echo
                    '
                    <form action="board.php" method="post">
                        <input type="hidden" name="hidden" value=' . $r['mesid'] . '>
                        <div class="row">
                            <div class="col">
                                <div class="card" style="margin-bottom: 10px;">     
                                    <h5 class="card-header">' . $r['user'] . '</h5>
                                    <div class="card-body">
                                        <p class="card-text" style="text-align:justify;">
                                        ' . $r['content'] . '
                                        </p>
                                        <p style="margin-top: 10px;color:#0080FF;">附加檔案：</p>
                                        <p>' . $filename2 . '</p>
                                        <button type="submit" class="btn btn-outline-secondary btn-sm" id="download" name="download" onclick="upmeid(' . $r['mesid'] . ')">下載檔案</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>';
                }
            }
            ?>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
    </body>

    </html>
<?php

} else {
    header("Location:index.php");
}
?>