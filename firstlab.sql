-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2022-07-01 10:17:32
-- 伺服器版本： 10.4.24-MariaDB
-- PHP 版本： 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `firstlab`
--

-- --------------------------------------------------------

--
-- 資料表結構 `file`
--

CREATE TABLE `file` (
  `fiid` int(11) NOT NULL,
  `messageid` int(11) NOT NULL,
  `filename` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `file`
--

INSERT INTO `file` (`fiid`, `messageid`, `filename`) VALUES
(95, 11, 'word檔測試2.docx'),
(96, 11, '文字檔2.txt'),
(97, 12, 'Cat03.jpg'),
(98, 13, '簡報測試1.pptx'),
(99, 14, 'pdf測試5.pdf'),
(100, 14, '文字檔5.txt'),
(130, 10, 'pdf測試1.pdf'),
(131, 10, 'word檔測試1.docx'),
(132, 10, '文字檔1.txt'),
(182, 28, 'Cat04.jpg');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `memid` int(11) NOT NULL,
  `user` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `member`
--

INSERT INTO `member` (`memid`, `user`, `password`) VALUES
(1, 'user1', 'abc123'),
(2, 'user2', 'zxc123'),
(3, 'user3', 'aabbcc12');

-- --------------------------------------------------------

--
-- 資料表結構 `message`
--

CREATE TABLE `message` (
  `mesid` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `content` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `message`
--

INSERT INTO `message` (`mesid`, `memberid`, `content`) VALUES
(10, 1, '測試修改留言6'),
(11, 1, '測試留言2'),
(12, 2, '測試留言3'),
(13, 3, '測試留言4'),
(14, 3, '測試留言5'),
(20, 1, '測試留言6'),
(28, 2, '測試留言8');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`fiid`),
  ADD KEY `messageid` (`messageid`);

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`memid`);

--
-- 資料表索引 `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`mesid`),
  ADD KEY `memberid` (`memberid`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `file`
--
ALTER TABLE `file`
  MODIFY `fiid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `member`
--
ALTER TABLE `member`
  MODIFY `memid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `message`
--
ALTER TABLE `message`
  MODIFY `mesid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- 已傾印資料表的限制式
--

--
-- 資料表的限制式 `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`messageid`) REFERENCES `message` (`mesid`);

--
-- 資料表的限制式 `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`memberid`) REFERENCES `member` (`memid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
